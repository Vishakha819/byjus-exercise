software:- -> node js should be installed on the system. 

to Run :- -> install node using "**npm i**"  in the project folder -> then run ng serve -o -> the project will be open in the default browser "http://localhost:4200"

project details : -->

For Job Search it will work in 4 ways
1. when  both location and experience is empty it will bring all the jobs
2. when only location is entered then the jobs will be filtered and displayed based on the location
3. when only experience is entered then the jobs will be filtered and displayed based on the experience
4. when both are entered it will filter the jobs based on the combination of both location and experience if
     no data found matching the entered location and experience error message will be displayed


*sortings*
 sortByLocation: All the jobs displayed will be sorted by Location
 sortBy Experience:  All the jobs displayed will be sorted by Experience
 
 Searching: when we enter anything in search text box it will filter the displayed jobs on the basis of skills matching the entered text

the project works fine in all kind of devices.(responsive web design);
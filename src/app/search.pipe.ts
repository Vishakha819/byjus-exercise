import { Pipe, PipeTransform } from '@angular/core';
import {JobModel} from 'src/DataModels/JobEntity'
@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(jobList: any, serachedTerm: any): JobModel {

    if (!jobList || !serachedTerm) {
      return jobList;
    }

    return jobList.filter(jobL => (jobL.skills.toLowerCase().indexOf(serachedTerm.toLowerCase()) !== -1))

  }

}

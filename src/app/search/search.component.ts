import { Component, OnInit } from '@angular/core';
import { JobServiceService } from 'src/app/job-service.service'
import { FormGroup, FormControl } from '@angular/forms'

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  jobs: any;
  result: Object[];
  finalResults = [];
  locationList: string[] = [];
  experienceList: string[] = [];
  searchForm: FormGroup;
  searchButtonClicked:boolean=false;
  constructor(private jobService: JobServiceService) {

  }

  ngOnInit() {
    this.searchForm = new FormGroup({
      exp: new FormControl(),
      loc: new FormControl()
    })
    this.getJobData();
  }
  getJobData() {
    this.jobService.getData().subscribe(result => {
      this.jobs = Object.values(result);

      result.jobsfeed.forEach((x) => {
        if (x.location != '') {
          this.locationList.push(x.location);
          this.locationList.unshift('');
          this.locationList = this.locationList.filter(this.uniqueValues)
        }
        if (x.experience != '') {
          this.experienceList.push(x.experience);
          this.experienceList.unshift('');
          this.experienceList = this.experienceList.filter(this.uniqueValues)
        }

      })
    })

  }
  uniqueValues(value, index, self) { return self.indexOf(value) === index; }

  SearchClick() {
   
    this.searchButtonClicked=true;
    this.finalResults = [];
    if(this.searchForm.value.loc== null){
      this.searchForm.value.loc="";
    }
    if(this.searchForm.value.exp== null){
      this.searchForm.value.exp="";
    }
    let location: string = (this.searchForm.value.loc);
    let experience: string = (this.searchForm.value.exp);
    this.result = this.jobs[0];
    if (location != "" && experience != "") {
      this.finalResults = this.result.filter(data => data['location'] == location && data['experience'] == experience);
    }
    else if (location == "" && experience != "") {
      this.finalResults = this.result.filter(data => data['experience'] == experience);
    }
    else if (location != "" && experience == "") {
      this.finalResults = this.result.filter(data => data['location'] == location);
    }
    else if ((location == "" && experience == "")) {
      this.finalResults = this.result;
    }

  }
}



import { Component, OnInit, Input } from '@angular/core';
import {JobModel} from 'src/DataModels/JobEntity'
@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css']
})
export class DisplayComponent implements OnInit {
@Input()
 JobsToDisplay :JobModel[];
 searchedKey:string;
  constructor() { }

  ngOnInit() {
  }

  sortByLocation() {
    this.JobsToDisplay.sort(function (a, b) {
      var x = a.location.toLowerCase();
      var y = b.location.toLowerCase();
      if (x < y) { return -1; }
      if (x > y) { return 1; }
     return 0;
    })
  }
  sortByExp() {
    this.JobsToDisplay.sort(function (a, b) {
      var x = a.experience.toLowerCase();
      var y = b.experience.toLowerCase();
      if (x < y) { return -1; }
      if (x > y) { return 1; }
      return 0;
      
    })
  }
}

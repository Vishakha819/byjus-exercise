import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';
import { DisplayComponent } from './display/display.component';
import {HttpClientModule} from '@angular/common/http';
import { NoDataToDisplayComponent } from './no-data-to-display/no-data-to-display.component';
import { SearchPipe } from './search.pipe'
@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    DisplayComponent,
    NoDataToDisplayComponent,
    SearchPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoDataToDisplayComponent } from './no-data-to-display.component';

describe('NoDataToDisplayComponent', () => {
  let component: NoDataToDisplayComponent;
  let fixture: ComponentFixture<NoDataToDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoDataToDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoDataToDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

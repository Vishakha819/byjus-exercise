import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-data-to-display',
  templateUrl: './no-data-to-display.component.html',
  styleUrls: ['./no-data-to-display.component.css']
})
export class NoDataToDisplayComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
